function rozniczki(theta, L, v0, mu)
	% funkcja rozwiązująca równanie różniczkowe dla wahadła
	% theta - kąt początkowy
	% L - długość ramienia
	% v - prędkość początkowa
	% mu - współczynnik oporu
	g = 9.8;

	t = linspace(0, 30, 10000);
	dt = t(end) - t(end-1);

	fi = t*0;
	fi(1) = theta;

	a = t*0;
	a(1) = -g/L*sin(theta);

	v = t*0;
	v(1) = v0;
	
	for i = 2:length(t)
		a(i) = -mu*v(i-1) -g/L*sin(fi(i-1));
		v(i) = v(i-1) + a(i)*dt;
		fi(i) = fi(i-1) + v(i)*dt;
	end

	figure, hold on, grid on
	plot(t, fi*180/pi, 'r');

	figure, hold on, grid on
	plot(t, v, 'b')

	figure, hold on, grid on
	plot(t, a, 'g');

	figure, hold on, grid on
	plot(fi, v, '-');
return
