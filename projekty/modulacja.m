function modulacja()
	%
	%
	%
	w0 = 1;
	x = linspace(-2, 2, 300);
	ym = x + sin(2*pi*x);
	y = sin(2*pi*ym);

	figure
	subplot(2, 1, 1);
	hold on, grid on;
	plot(x, y);

	subplot(2, 1, 2);
	hold on, grid on;
	plot(x, cos(2*pi*x));

return
