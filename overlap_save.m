function overlap_save()
    %
    %
    %
    N = 500;
    n = 0:N-1;
    x = sin(2*pi*1/8*n) + sin(2*pi*3/8*n);
%     figure, hold on, grid on
%         plot(n,x)
    
    R = 30;
    h = fir1(R,1/2,hamming(R+1));
    [H F] = freqz(h,1,1024,1);
    [X Fx] = freqz(x,1,1024,1);
    figure, hold on, grid on
        plot(F,abs(H),'b')
        plot(Fx,2*abs(X)/N,'r')
    
    y = zeros(1,N+R);
    x_pom = [zeros(1,R), x];
    h = flip(h);
   
    n_pom = 0:N+R-1;
   
    for i = 1:N;
            y(i) = sum(x_pom(i:R+i).*h);
    end
    
    for i = 1:R-1
            y(N+i) = sum(x_pom(N+i:N+R-1).*h(1:R-i));
    end    
   
    figure, hold on, grid on
    plot(n_pom, y);
   
    [Y Fy] = freqz(y, 1, 1024, 1);
    figure, hold on, grid on;
        plot(F, abs(H), 'b');
        plot(Fy, 2*abs(Y)/(N), 'r')
    
            