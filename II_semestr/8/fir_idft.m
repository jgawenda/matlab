function fir_idft()
	% jak co, to zadawajcie pytania. Serio.
	% funkcja realizująca filtr FIR za pomocą metody okien czasowych połączoną z IDFT
	% zapisywanie podstawowych danych
	fp = 1250; % częstotliwość przenoszenia
	fr = 1750; % częstotliwość zaporowa
	Rr = 50; % tłumienie w pasmie zaporowym
	fs = 8000; % częstotliwość próbkowania
	K = 4; % stała dla okna Hamminga

	% obliczanie częstotliwości 3dB
	fo = (fp+fr)/2;

	% funkcja Sypki do kreślenia charakterystyki częstotliwościowej
	% fir_1;

	% obliczanie unormowanych częstotliwości
	FP = fp/fs;
	FR = fr/fs;
	FO = fo/fs;

	% obliczanie minimalnego, optymalnego rzędu filtru
	N = ceil(K/(FR-FP));

	% projektowanie odpowiedzi impulsowej za pomocą genrowania jej widma amplitudowego
	% generowanie wektora częstotliwości unormowanych
	F = (0:(N-1))/N;

	% generowanie widma amplitudowego
	% H1 = F <= FO - ta linijka zwraca '1' dla każdej wartości, która jest mniejsza bądź równa FO
	% z tego otrzymujemy lewą połówkę widma
	% oznacza to, że prążki dla częstotliwości mniejszych od F mają wartość '1'
	% z tego wynika, że te częstotliwości będą przenoszone, a reszta zostanie odcięta
	H1 = F <= FO;

	% flip(H1, 2) robi odbicie lustrzane H1, przez co łatwiej jest nam wykonać drugą połówkę widma
	H2 = flip(H1, 2);
	% circshift(H2, 1) przesuwa wszystkie elementy wektora o jedną pozycję w prawo
	% ostatni element staje się pierwszy, itd.
	% trzeba pamiętać, że prawa strona ma jeden element mniej! (bo jest on w częstotliwości fs)
	H2 = circshift(H2, 1);
	% zerujemy pierwszy element H2, ponieważ jest z lewej strony
	H2(1) = 0;
	H = H1 + H2;

	% obliczamy odpowiedź impulsową i za pomocą funkcji fftshift() 'środkujemy' ją
	h = ifft(H);
	h = fftshift(h);
	% korzystamy z okna Hamminga, aby wytłumić listki boczne
	h = h.*hamming(N).';

	% generujemy odpowiedź częstotliwościową dla filtru
	% [H1 F1] = freqz(h, 1, 1000, fs);
	% fir_3;

	% generujemy przykładowy sygnał
	t = 0:1/fs:400/fs;
	x = sin(2*pi*450*t) + sin(2*pi*1200*t) + sin(2*pi*1800*t) + 2*sin(2*pi*3150*t);

	% filtrujemy sygnał za pomocą splotu liniowego x(t)*h(t)
	xh = conv(h, x);
	xh = xh(1:length(x));

	% obliczamy widmo częstotliwościowe, aby zobaczyć efekt filtru
	XH = fft(xh);
	X = fft(x);
	M = length(X);

	% rysujemy wszystkie potrzebne wykresy
	figure, hold on, grid on;
		title('odpowiedź impulsowa filtru dolnoprzepustowego');
		stem((0:(N-1)), h, 'b');

	figure
		stem(F, abs(H), 'b');
		title('widmo amplitudowe filtru dolnoprzepustowego');
		grid on;

	figure, hold on, grid on;
		title('widmo częstotliwościowe sygnału x i przefiltrowanego sygnału xh (dolnoprzepustowy)');
		stem((0:M-1)*fs/M, abs(X), 'b');
		stem((0:M-1)*fs/M, abs(XH), 'r');

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GÓRNO PRZEPUSTOWY

	% na podstawie widma amplitudowego dla filtru dolnoprzepustowego możemy zaprojektować filtr górnoprzepustowy
	% wtedy dla częstotliwości (F1, 1/2) mamy '1', czyli te częstotliwości są przenoszone
	HG = 1 - H;

	% obliczanie odpowiedzi impulsowej filtru za pomocą IDFT
	hg = ifft(HG);
	hg = fftshift(hg);
	hg = hg.*hamming(N).';

	% filtrowanie sygnału za pomocą splotu liniowego
	xh = conv(hg, x);
	xh = xh(1:length(x));

	% obliczanie widma amplitudowego dla sygnalu przefiltrowanego xh
	XH = fft(xh);

	% rysowanie wykresów
	figure, hold on, grid on;
		title('odpowiedź impulsowa filtru górnoprzepustowego')
		stem((0:N-1), hg, 'b');

	figure
		stem(F, abs(HG), 'b');
		title('widmo amplitudowe filtru górnoprzepustowego');
		grid on;

	figure, hold on, grid on;
		title('widmo częstotliwościowe sygnału x i przefiltrowanego sygnału xh (górnoprzepustowy)')
		stem((0:M-1)*fs/M, abs(X), 'b');
		stem((0:M-1)*fs/M, abs(XH), 'r');

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PASMOWO PRZEPUSTOWY

	% zapisywanie częstotliwości minimalnej i maksymalnej dla filtru
	f2 = 2500;
	f1 = 1500;

	% zapisywanie częstotliwości unormowanych F1 i F2
	F1 = f1/fs;
	F2 = f2/fs;

	% generowanie widma amplitudowego dla filtru dolnoprzepustowego, o częstotlwości przepustowej f1
	H1L = F <= F1;
	H1P = flip(H1L, 2);
	H1P = circshift(H1P, 1);
	H1P(1) = 0;
	H1 = H1L + H1P;

	% generowanie widma aplitudowego dla filtru dolnoprzepustowego, o częstotlwości przepustowej f2
	H2L = F <= F2;
	H2P = flip(H2L, 2);
	H2P = circshift(H2P, 1);
	H2P(1) = 0;
	H2 = H2L + H2P;

	% CAŁY CZAS PRACUJEMY NA CZĘSTOTLIWOŚCIACH UNORMOWANYCH!!!
	% F2 > F1, dlatego od widma amplitudowego dla F2 odejmujemy widmo dla F1
	% czyli wtedy zostaje nam przedział częstotliwości (F1, F2) dla których prążki mają wartość 1
	% dla reszty częstotliwości prążki mają wartość 0, czyli te częstotliwości są 'wycinane'
	HPP = H2 - H1;

	% obliczanie odpowiedzi impulsowej
	hpp = ifft(HPP);
	hpp = fftshift(hpp);
	hpp = hpp.*hamming(N).';

	% filtrowanie sygnału
	xh = conv(hpp, x);
	xh = xh(1:length(x));

	XH = fft(xh);

	% rysowanie wykresów
	figure, hold on, grid on;
		title('odpowiedź impulsowa filtru pasmowoprzepustowego');
		stem((0:N-1), hpp, 'b');

	figure
		stem(F, abs(HPP), 'b');
		title('widmo amplitudowe filtru pasmowoprzepustowego');
		grid on;

	figure, hold on, grid on;
		title('widmo częstotliwościowe sygnału x i przefiltrowanego sygnału xh (pasmowoprzepustowy)');
		stem((0:M-1)*fs/M, abs(X), 'b');
		stem((0:M-1)*fs/M, abs(XH), 'r');

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PASMOWO ZAPOROWY

	% robiony jest na podstawie filtru pasmowo pasmowoprzepustowego
	% jedynki wtedy zostają dla wszystkich częstotliwości, oprócz tych wycinanych
	% czyli wzmocnienie dla częstotliwości (0, f1)u(f2, fs/2) jest równe 1
	% dla częstotliwośći (f1, f2) wynosi zero, czyli są wycinanane
	HPZ = 1 - HPP;

	% obliczanie odpowiedzi impulsowej, tak jak w poprzednich przypadkach
	hpz = ifft(HPZ);
	hpz = fftshift(hpz);
	hpz = hpz.*hamming(N).';

	% filtrowanie sygnału
	xh = conv(hpz, x);
	xh = xh(1:length(x));

	XH = fft(xh);

	% rysowanie wykresów
	figure, hold on, grid on;
		title('odpowiedź impulsowa filtru pasmowozaporowego');
		stem((0:N-1), hpz, 'b');

	figure
		stem(F, abs(HPZ), 'b');
		title('widmo amplitudowe filtru pasmowozaporowego');
		grid on;

	figure, hold on, grid on;
		title('widmo częstotliwościowe sygnału x i przefiltrowanego sygnału xh (pasmowozaporowy)');
		stem((0:M-1)*fs/M, abs(X), 'b');
		stem((0:M-1)*fs/M, abs(XH), 'r');
return
