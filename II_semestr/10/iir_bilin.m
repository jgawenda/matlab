function iir_bilin()
	% WZÓR Z ZIELIŃSKIEGO, BO PEWNIE NIEKTÓRYM OSOBOM NIE BĘDZIE SIĘ CHCIAŁO TEGO SPRAWDZIĆ
	% STR. 301
	% STOSUJECIE TO DLA KAŻDEGO ZERA I BIEGUNU
	%
	% 				ZERO DLA FILTRU CYFROWEGO, JEŚLI LICZYCIE NOWE ZERO
	% 				LUB BIEGUN, JEŚLI LICZYCIE NOWY BIEGUN
	% 						      |
	% 						      v
	% 					          2*fs + zk
	% 			 		     z - -----------
	% 		  z - 1 		          2*fs - zk
	% s - zk = 2*fs* ------- - zk = (2*fs - zk) -----------------
	% 		  z + 1 		         z + 1
	% 				^ 	           ^
	% 				| DODATKOWY BIEGUN, JEŚLI LICZYCIE NOWE ZERO, (LUB DODATKOWE ZERO, JEŚLI LICZYCIE NOWY BIEGUN)
	% 				|
	% 				|
	% 	TO ZMIENIA WZMOCNIENIE (JEŚLI LICZYCIE NOWY BIEGUN, WZMOCNIENIE JEST DZIELONE PRZEZ TEN CZYNNIK,
	% 				JEŚLI LICZYCIE NOWE ZERA, WZMOCNIENIE JEST MNOŻONE PRZEZ TEN CZYNNIK)
	%
	% WIEM, ŻE NIE JEST TO ZBYT INTUICYJNE, ALE JESTEŚCIE W STANIE TO OGARNĄĆ
	% BO SERIO, TRUDNO TO WYTŁUMACZYĆ OT TAK
	%
	% projektowanie filtru IIR za pomocą analogowego prototypu Butterwortha i transformacji biliniowej
	Rr = 50; % tłumienie w pasmie zaporowym
	Rp = 2; % tłumienie w pasmie przepustowym
	fp = 1250; % częstotliwość przenoszenia
	fr = 1750; % częstotliwość zaporowa
	fs = 8000; % częstotliwość próbkowania

	% obliczanie parametrów potrzebnych do zaprojektowania prototypu
	% Zieliński - str. 147-148
	% innymi słowy, tak jak wcześniej
	N = ceil(log10((10^(Rr/10)-1) / (10^(Rp/10)-1))/(2*log10(fr/fp)));
	f3dB = fr / ((10^(Rr/10)-1)^(1/(2*N)));

	% pulsacja unormowana trzydecybelowa
	W3dB = 2*pi*f3dB/fs;
	fo = f3dB;

	% przekształcenie (poprawka) częstotliwości. Przez nią nie wychodzi charakterystyka częstotliwościowa filtru analogowego
	% ale za to wyjdzie charakterystyka częstotliwościowa dla filtrui cyfrowego
	w3dB = 2*fs*tan(W3dB/2);

	iir_1

	% generowanie biegunów dla prototypu, tak jak wcześniej
	k = 1:N;
	pk = exp(j*(pi/2 + pi/(2*N) + (k-1)*pi/N));
	pk = w3dB * pk;

	% prototyp Butterwortha nie ma zer. Ma wzmocnienie Ka, będące iloczynem biegunów
	% i ma bieguny zawarte w wektorze Pa
	Ka = prod(-pk);
	Pa = pk;

	% obliczanie współczynników filtru za pomocą funkcji zp2tf
	[ba aa] = zp2tf([], Pa, Ka);
	% obliczanie charakterystyki częstotliwościowej filtru analogowego
	[Ha Fa] = freqs(ba, aa, 2*pi*linspace(0, fs, 10000));
	iir_2

	% obliczanie zer i biegunów dla filtru cyfrowego za pomocą transformacji biliniowej
	% wzmocnienie filtru cyfrowego jest na początku takie samo jak analogowego
	Kc = Ka;
	Pc = [];
	% wzór do transformacji biliniowej jest w Zielińskim - str. 301
	% dużo do napisania, lepiej to zobaczyć na papierze
	% w tym przypadku (bo prototyp nie ma zer), obliczamy tylko nowe bieguny
	for i = 1:length(Pa)
		% pk - k-ty biegun filtru cyfrowego
		% zk - k-ty biegun filtru analogowego
		%
		% 	  2*fs - zk
		% pk = - -----------
		% 	  2*fs + zk
		%
		Pc = [Pc, (2*fs + Pa(i))/(2*fs - Pa(i))];

		% wzmocnienie dla filtru cyfrowego jest zmodyfikowanym wzmocnieniem filtru analogowego
		% w tym przypadku wzmocnienie filtru analogowego dzielimy przez (2*fs - pk) za każdy biegun
		Kc = Kc/(2*fs - Pa(i));
	end
	% nowe zera dla filtru cyfrowego, patrz wzór
	Zc = -ones(length(Pc), 1);

	% obliczanie współczynników filtru cyfrowego za pomocą funkcji zp2tf()
	[b1 a1] = zp2tf(Zc, Pc, Kc);
	% obliczanie charakterystyki częstotliwościowej dla filtru cyfrowego
	[H1 F1] = freqz(b1, a1, 4096, fs);
	iir_3

	% pierdoły do odpowiedzi impulsowej
	% siedziałem parę godzin nad tymi zadaniami, cholernie nie chce mi się tego robić
	% ogarniecie sami, mam nadzieję
	% bo to odrobinę nie wychodzi
	w3dB = 2*pi*f3dB;
	pk = exp(j*(pi/2 + pi/(2*N) + (k-1)*pi/N));
	pk = w3dB * pk;

	Ka = prod(-pk);
	Pa = pk;

	[ba aa] = zp2tf([], Pa, Ka);

	[ha, wa, ta] = impulse(ba, aa, linspace(0, 10e-3, 10000));
	[h1, t1] = impz(b1, a1, floor(10e-3*fs), fs);
	figure(4), hold on, grid on;
		plot(ta, ha, 'b', 'linewidth', 4);
		stem(t1, h1*fs, 'gd', 'fill', 'linewidth', 3);

return
