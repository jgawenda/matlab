function iir_but()
	% jakby coś było niejasne, to piszcie do mnie, ślijcie maile, cokolwiek
	% ale kurka pytajcie, żeby nie było, że wrzuciłem kody i tyle z tego było >:C
	% 
	% funkcja projektująca filtr rekursywny za pomocą filtru Butterwortha
	% i metody niezmienności odpowiedzi impulsowej
	% wypisywanie podstawowych wartości
	Rp = 2; % tłumienie w pasmie przepustowym
	Rr = 50; % tłumienie w pasmie zaporowym
	fp = 1250; % częstotliwość przenoszenia
	fr = 1750; % częstotliwość zaporowa
	fs = 8000; % częstotliwość próbkowania
	fo = (fp+fr)/2; % nie wiem co, ale może się przydać?
	T = 1/fs; % okres próbkowania


	% wyliczanie rzędu filtru za pomocą wzoru z Zielińskiego - str. 147
	N = ceil(log10((10^(Rr/10)-1) / (10^(Rp/10)-1)) / (2*log10(fr/fp)));

	% wyliczanie częstotliwości 3-decybelowej, ta sama strona
	f3dB = fr / ((10^(Rr/10)-1)^(1/(2*N)));
	w3dB = 2*pi*f3dB;
	fo = f3dB;

	iir_1

	% liczenie miejsc zerowych dla obliczonych wyżej wartości
	% wzór w Zielińskim - str. 148
	k = 1:N;
	pk = exp(j*(pi/2 + pi/(2*N) + (k-1)*pi/N));
	pk = w3dB * pk;

	% obliczanie współczynników transmitancji analogowego prototypu
	K = prod(-pk);
	P = pk;

	% zp2tf() zamienia zera, bieguny i wzmocnienie na współczynniki transmitancji
	% ba - współczynniki licznika
	% aa - współczynniki mianownika

	[ba aa] = zp2tf([], P, K);
	% obliczanie odpowiedzi częstotliwościowej dla filtru analogowego
	[Ha Fa] = freqs(ba, aa, 2*pi*linspace(0, fs, 10000));
	iir_2

	% obliczanie współczynników dla filtru cyfrowego
	% Zieliński - str. 297, wzór 11.12
	% funkcja residue() zamienia duży iloraz dwóch wielomianów na sumę ułamków prostych
	% w razie czego zobaczcie sobie na jej opis
	[Ra Pa Ka] = residue(ba, aa);

	% Rc - kolejne liczniki ułamków prostych
	Rc = T*Ra;
	% Pc - miejsca zerowe ułamków prostych dla filtru cyfrowego
	Pc = exp(Pa*T);

	% moja funkcja, która zwraca współczynniki transmitancji filtru cyfrowego
	[b1 a1] = make_poly(Rc, Pc, Ka);
	% wyliczanie odpowiedzi częstotliwościowej filtru cyfrowego
	[H1 F1] = freqz(b1, a1, 4096, fs);
	iir_3

	% wyliczanie i rysowanie odpowiedzi impulsowej filtru analogowego i filtru cyfrowego
	[ha, wa, ta] = impulse(ba, aa, linspace(0, 10e-3, 1000));
	[h1, t1] = impz(b1, a1, floor(10e-3*fs), fs);
	figure(4), hold on, grid on;
		plot(ta, ha, 'b', 'linewidth', 4);
		stem(t1, h1*fs, 'rs', 'fill', 'linewidth', 3);

	% nie jestem pewien tych wykresów, lepiej je pominąć
	[q s k] = tf2zp(ba, aa); 
	figure
		zplane(q, s);
		title('rozkład zer i biegunów dla filtru analogowego')

	[z1 p1 k1] = tf2zp(b1, a1);
	figure
		zplane(z1, p1);
		title('rozkład zer i biegunów dla filtru cyfrowego')

	% testowanie filtru na sygnale
	% generowanie próbek sygnału
	t = 0:1/fs:200/fs;
	x = sin(2*pi*450*t) + sin(2*pi*1200*t) + sin(2*pi*1800*t) + sin(2*pi*3150*t);
	M = length(x);
	% generowanie indeksów częstotliwości do widma sygnału
	F = (0:M-1)*fs/M;

	% filtrowanie sygnału i ucinanie go
	xh = conv(x, h1);
	xh = xh(1:length(x));

	% obliczanie widma sygnału x i sygnału przefiltrowanego xh
	X = fft(x);
	XH = fft(xh);

	% rysowanie wyniku, widać, że filtr działą
	figure, hold on, grid on
		stem(F, abs(X), 'b');
		stem(F, abs(XH), 'r');

return

function [B A] = make_poly(R, P, K)
	% działa! :D
	% funkcja zamieniająca sumę ułamków prostych na jeden ułamek
	% ------------- WEJŚCIA -------------
	% R - wektor kolejnych wartości licznika
	% P - wektor biegunów
	% K - reszta
	% ------------- WYJŚCIA -------------
	% B - wektor współczynników licznika
	% A - wektor współczynników mianownika
	%
	% 	B(n)       R(1)         R(2)               R(N)
	%      ------ = ---------- + ---------- + ... + ---------- + K
	% 	A(n)     s - P(1)     s - P(2)           s - P(N)
	%

	% A jest wyliczone za pomocą funkcji poly(Z), która zwraca współczynniki wielomianu dla zadanych biegunów
	A = poly(P);

	% B jest cięższe do zrobienia
	% wrzucę zdjęcie dla rozpisania
	B = [];
	if isempty(K)
		for i = 1:length(R)
			% P_pom to wektor pomocniczy, w którym znajdują się wszystkie miejsca zerowe oprócz i-tego
			% jest dlatego, że rozszerzamy licznik i mianownik i-tego ułamka o iloczyn pozostałych mianowników
			% jeśli usuniemy i-ty biegun, zostają nam bieguny pozostałe
			% wtedy przy użyciu funkcji poly(P_pom) dostajemy współczynniki dla ich iloczynu
			% następnie mnożymy je przez licznik i-tego ułamka i dodajemy do macierzy B jako jeden wiersz
			P_pom = P;
			P_pom(i) = [];
			B = [B; R(i) * poly(P_pom)];
		end
	else
		% to jest w przypadku, gdy reszta istnieje
		% ale na szczęście, nie często się to dzieje
		% CHYBA
		for i = 1:length(R)
			P_pom = P;
			P_pom(i) = [];
			B = [B; 0, R(i) * poly(P_pom)];
		end
		B = [B; K * poly(P)];
	end
	% sum(B) zwraca wektor, który jest sumą wszystkich wierszy macierzy B
	B = sum(B);
return
