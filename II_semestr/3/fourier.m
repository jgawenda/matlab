function fourier()
	fo = 3000; % częstotliwość sygnału
	fs = 8000; % częstotliwosć próbkowania
	N = 10; % liczba próbek

	% generowanie sygnału "analogowego"
	t = linspace(0, 3/fo, 1000);
	y = 1/4 + sin(2*pi*fo*t);

	% generowanie próbek
	n = 0:1/fs:(N-1)/fs;
	x = 1/4 + sin(2*pi*fo*n);

	% rysowanie sygnału i pobranych próbek
	figure(1), hold on, grid on;
		plot(t, y, 'b');

	% obliczanie DFT za pomocą własnej funkcji
	[F, X] = mdft(x, fs);
	% obliczanie DFT za pomocą funkcji wbudowanej
	XR = fft(x);

	% rysowanie widma sygnału
	figure 
		subplot(2, 1, 1)
		hold on, grid on;
		title('charakterystyka amplitudowa sygnału')
		stem(F, abs(X));
		stem(F, abs(XR));
		stem(fs/2, 0, 'xr', 'linewidth', 2)

	% faza ma się zgadzać w punktach, gdzie charakterystyka amplitudowa ma wartości różne od zera
		subplot(2, 1, 2)
		hold on, grid on;
		title('charakterystyka fazowa sygnału')
		stem(F, atan2(imag(X), real(X)));
		stem(F, atan2(imag(XR), real(XR)));
	
	% transformacja odwrotna
	x = midft(X);
	xr = ifft(XR);

	figure(1)
		stem(n, real(x), 'r') % jest real(x), bo matlab gada o wartościach urojonych (których nie ma >:C)
		stem(n, xr, 'k');

	% porównanie ciągłego widma sygnału, a jego dyskretnej wersji
	% DFT vs CFT

	% jeśli dodamy wystarczająco dużo zer na koniec wektora próbek (zero-padding), jego widmo będzie przypominało
	% widmo ciągłe
	XC = fft(x, 2048);
	figure, hold on, grid on;
		plot(linspace(0, fs, length(XC)), abs(XC), 'b');
		stem(F, abs(X), 'r');

return

% działa
function [F, X] = mdft(x, fs)
	% zwraca wektor indeksów częstotliwości F i wektor widma sygnału X
	% x - wektor próbek
	% fs - częstotliwość próbkowania

	N = length(x);
	k = 0:(N-1);
	n = k.';

	% generowanie macierzy W, potrzebnej do obliczenia widma sygnału (X = W*x.')
	% drugi wykład Kowalczyka?
	% Zieliński, str. 238

	% w ogóle, jest taka mała sztuczka
	% jeśli zrobimy mnożenie element po elemencie ('.*') wektora wierszowego z kolumnowym, to otrzymamy macierz
	% gdzie kolejne wiersze są ilocznem pierwszego wektora z kolejnymi elementami drugiego wektora
	w = k.*n;
	W = exp(-2*pi*j*w/N);

	F = fs/N*k;
	X = W*x.';
return

% działa
function x = midft(X)
	% zwraca wektor próbek x
	% X - widmo sygnału

	N = length(X);
	k = 0:(N-1);
	n = k.';

	% generowanie macierzy W, patrz wyżej
	w = k.*n;
	W = exp(2*pi*j*w/N);

	x = 1/N*W*X;
return
