function okna()
	% projektowanie filtru FIR z użyciem metody okien czasowych
	fp = 1250; % częstotliwość przenoszenia
	fr = 1750; % częstotliwosć zaporowa
	Rr = 50; % tłumienie w pasmie zaporowym
	fs = 8000; % częstotliwość próbkowania
	K = 4; % stała dla okna Hamminga

	% zapisywanie częstotliwości unormowanych 
	FP = fp/fs;
	FR = fr/fs;

	% funkcja Sypki do kreślenia charakterystyki w skali liniowej i decybelowej
	fir_1

	% wyliczanie rzędu filtru, Zieliński - str. 336
	N = ceil(K/(FR-FP));
	% projektowanie filtru dolnoprzepustowego
	% fo - częstotliwość 3dB, FO - unormowana częstotliwość 3dB
	fo = (fp+fr)/2;
	FO = fo/fs;

	% generowanie wektoru n, składający się z liczb całkowitych, o długości N
	% chodzi o to, żeby był jak najbardziej symetryczny
	if mod(N, 2) == 0
		n = (-N/2+1):N/2;
	else 
		n = -(N-1)/2:(N-1)/2;
	end

	% generowanie odpowiedzi impulsowej dla zadanej częstotliwości
	% wzory na odpowiedzi impulsowe są zawarte w Zielińskim - str. 335
	h = 2*FO*sinc(2*FO*n);

	% następnie stosujemy okno Hamminga na naszej odpowiedzi
	% funkcja hamming(N) zwraca N wartości dla okna Hamminga. Te wartości są symetryczne
	% hamming(N) zwraca wektor KOLUMNOWY, musicie go transponować, jeśli chcecie robić mnożenie element po elemencie
	h = h.*hamming(length(h)).';

	% generowanie widma odpowiedzi częstotliwościowej
	% nie wiem tylko czemu nie przechodzi przez wyznaczony punkt :C
	[H1 F1] = freqz(h, 1, 1000, fs);
	fir_3	

	% pokazanie filtrowania na podstawie zadanego sygnału
	t = 0:1/fs:400/fs;
	x = sin(2*pi*450*t) + sin(2*pi*1200*t) + sin(2*pi*1800*t) + 2*sin(2*pi*3150*t);

	% splot linowy i obcięcie wektora xh aby jego długość była równa długości wektora t
	% można obciąć z drugiej strony, ale i tak widać, że działa
	xh = conv(h, x);
	xh = xh(1:length(x));

	% rysowanie sygnału chociaż wygląda tak sobie
	figure, hold on, grid on;
		plot(t, x, 'b');
		plot(t, xh, 'r');

	% sprawdzenie widma sygnału x i przefiltrowanego sygnału xh
	XH = fft(xh);
	X = fft(x);
	M = length(X);

	% jak widać, w widmie sygnału xh brakuje dwóch prążków, które zostały odfiltrowane
	% więc, no, działa
	figure, hold on, grid on;
		stem((0:M-1)*fs/M, abs(X), 'b');
		stem((0:M-1)*fs/M, abs(XH), 'r');
return
