function chka()
	% funkcja rysująca charakterystyki amplitudowe i fazowe dla zadanych transmitancji
	% zapisywanie współczynników licznika i mianownika
	b1 = [2 3];
	a1 = [1 -1/2];

	b2 = [2 3];
	a2 = [1 1/2];

	b3 = [2 -1];
	a3 = [1 3/2 -1/2];

	b4 = [2 -1];
	a4 = [1 1/2 -1/2];

	% wektor indeksów częstotliwości
	f = linspace(-2*pi, 2*pi, 1000);

	% przykładowa realizacja obliczania charakterystyki amplitudowej i fazowej bez korzystania z funkcji wbudowanych
	% l1 - kolejne wartości licznika transmitancji
	% m1 - kolejne wartości mianownika transmitancji
	l1 = 2 + 3*exp(-j*f);
	m1 = 1 - 1/2*exp(-j*f);

	% obliczanie wartości transmitancji jako dzielenie wartości licznika przez wartości mianownika
	H1 = l1./m1;

	% rysowanie charakterystyki dla pierwszej transmitancji
	% to u góry to charakterystyka amplitudowa
	% u dołu to charakterystyka fazowa
	figure(1)
		subplot(2, 1, 1);
		plot(f, abs(H1));
		grid on;

		subplot(2, 1, 2)
		plot(f, atan2(imag(H1), real(H1)));
		grid on;

	% liczenie transmitancji za pomocą zdeklarowanej niżej funkcji
	H2 = transmitancja(b2, a2, f);
	H3 = transmitancja(b3, a3, f);
	H4 = transmitancja(b4, a4, f);

	% rysowanie pozostałych charakterystyk
	figure(2)
		subplot(2, 1, 1)
		plot(f, abs(H2));
		grid on;

		subplot(2, 1, 2);
		plot(f, atan2(imag(H2), real(H2)));
		grid on;
	
	figure(3)
		subplot(2, 1, 1)
		plot(f, abs(H3));
		grid on;

		subplot(2, 1, 2);
		plot(f, atan2(imag(H3), real(H3)));
		grid on;
	
	figure(4)
		subplot(2, 1, 1)
		plot(f, abs(H4));
		grid on;

		subplot(2, 1, 2);
		plot(f, atan2(imag(H4), real(H4)));
		grid on;
return

function h = transmitancja(b, a, f)
	% funkcja licząca widmo dla zadanej czestotliwości
	licznik = f*0;
	mianownik = f*0;

	for i = 1:length(a)
		mianownik = mianownik + a(i)*exp(-j*(i-1)*f);
	end

	for i = 1:length(b)
		licznik = licznik + b(i)*exp(-j*(i-1)*f);
	end

	h = licznik./mianownik;
return
