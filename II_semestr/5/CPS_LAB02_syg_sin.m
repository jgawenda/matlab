function [s, x, sa] = CPS_LAB02_syg_sin(k,fo,fs)
%################################################################
%# CPS_LAB02_syg_sin.m											#
%# Funkcja demonstruj�ca pr�bkowanie							#
%# AGH, CPS, Krak�w, dn. 11-03-2019								#
%################################################################
%# Dane wej�ciowe:												#
%#	k  - liczba pe�nych okres�w "symetrycznych wzgl�dem to=0"	#
%#		 sygna�u sinusoidalnego									#
%#	fo - cz�stotliwo�� sygna�u sinusoidalnego					#
%#	fs - cz�stotliwo�� pr�bkowania								#
%#																#
%# Dane wyj�ciowe:												#
%#	s  - wygenerowany sygna� "analogowy"						#
%#	x  - sygna� cyfrowy (spr�bkowany sygna� analogowy s)		#
%#	sa - odtworzony sygna� "analogowy" z tw. Shannona			#
%#																#
%# Przyk�adowe wywo�anie:										#
%#	[s, x, sa] = CPS_LAB02_syg_sin(3,1000,5000);				#
%################################################################

close all;

% Generacja dziedziny t (symulacja liczb rzeczywistych)
t = linspace(-(k+1)/fo,(k+1)/fo,randi([1000 5000],1,1));


% Generacja sygna�u "analogowego"
tx=k/fo;
s = sin(2*pi*fo*t) .* imp_prost(t/(2*tx));


% P R � B K O W A N I E (miejsce na Pa�stwa kod)






% O D T W A R Z A N I E   S Y G N A � U   "A N A L O G O W E G O"
% T W I E R D Z E N I E   S H A N N O N A   (miejsce na Pa�stwa kod)
% w celach testowych zaleca si� aby odtworzony sygna� zapisa� w zmiennej 'sa'







% Test
if exist('sa','var') & ~isnan(fs)
	RMSE = sqrt(sum((s-sa).^2)./length(s));
	disp(['RMSE = ' num2str(RMSE)])
end	

% Generacja wykresu
figure, grid on, hold on
	set(gcf,'outerposition',get(0,'screensize')+[0 60 0 -70])
	set(gca,'linewidth',2,'box','on','fontsize',24,'fontweight','bold')
	set(gca,'position',[0.06 0.12 0.90 0.81])
	xlabel('Czas [s]','fontsize',24,'fontweight','bold')
	
	plot(t,s,'r','linewidth',4)

	
	
	

if ~exist('x','var')
	x = NaN;
end
if ~exist('sa','var')
	sa = NaN;
end

return
%===============================================================%
% EOF - CPS_LAB02_syg_sin.m										%
%===============================================================%