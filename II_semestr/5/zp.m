function zp()
	% funkcja rysująca rozkład zer i biegunów na płaszczyznie zespolonej
	% zapisywanie współczynników transmitancji
	b1 = [2 3];
	a1 = [1 -1/2];

	b2 = [2 3];
	a2 = [1 1/2];

	b3 = [2 -1];
	a3 = [1 3/2 -1/2];

	b4 = [2 -1];
	a4 = [1 1/2 -1/2];

	% wyliczanie zer i biegunów za pomocą funkcji wbudowanej tf2zp(B, A);
	% funkcja ta zwraca zera, bieguny i wzmocnienie dla zadanych współczynników transmitancji
	% B - współczynniki licznika
	% A - współczynniki mianownika
	[z1 p1 k1] = tf2zp(b1, a1);
	[z2 p2 k2] = tf2zp(b2, a2);
	[z3 p3 k3] = tf2zp(b3, a3);
	[z4 p4 k4] = tf2zp(b4, a4);

	% inna metoda obliczania zer i biegunów 
	% (za pomocą funkcji roots(), która zwraca miejsca zerowe po podaniu współczynników)
	% coś takiego działa tylko wtedy, gdy rząd licznika jest równy rządowi mianownika
	% gdy rząd licznika jest większy, na koniec współczynników mianownika należy dodać zera
	% tak mi się wydaje?
	% tak czy siak, tutaj bym sprawdził i nie polegał na mnie
	z1m = roots(b1);
	p1m = roots(a1);
	z1 == z1m
	p1 == p1m

	% rysowanie zer i biegunów za pomocą funkcji zplane()
	figure
	zplane(z1, p1);
	title('odpowiedź stabilna')

	figure
	zplane(z2, p2);
	title('odpowiedź stabilna')

	figure
	zplane(z3, p3);
	title('odpowiedź niestabilna')

	figure
	zplane(z4, p4);
	title('odpowiedź na granicy stabilności')
	% chyba tyle?
return
