function systemy()
	fs = 100; % częstotliwosć próbkowania
	N = 200; % liczba próbek
	K = 41; % liczba współczynników

	% generowanie indeksów czasowych dla sygnału x(t), filtru h(t) i wyniku splotu x(t)*h(t)
	n = 0:1/fs:(N-1)/fs;
	k = 0:1/fs:(K-1)/fs;
	% długość splotu - N+K-2
	ks = 0:1/fs:(K+N-2)/fs;

	% generowanie wartości próbek
	x = imp_prost((n-0.9)/.99);
	% współczynniki filtru
	y1 = h1(k, fs);
	y2 = h2(k, fs);
	y3 = h3(k, fs);

	% obliczanie splotu liniowego filtru h(t) z sygnałem x(t)
	x1 = conv(x, y1);
	x2 = conv(x, y2);
	x3 = conv(x, y3);

	% rysowanie sygnału
	figure, hold on, grid on;
		plot(n, x);

	% rysowanie współczynników filtru
	figure
		subplot(3, 1, 1)
		grid on;
		plot(k, y1);

		subplot(3, 1, 2)
		grid on;
		plot(k, y2);

		subplot(3, 1, 3)
		grid on;
		plot(k, y3);

	% rysowanie wyniku splotu filtru z sygnałem
	figure
		subplot(3, 1, 1)
		grid on;
		plot(ks, x1);

		subplot(3, 1, 2)
		grid on;
		plot(ks, x2);

		subplot(3, 1, 3)
		grid on;
		plot(ks, x3);
return

% funkcje obliczające współczynniki filtrów dla zadanych wzorów
function y = h1(t, fs)
	y = 1/20 * sinc(fs/20 * (t - 0.2));
return

function y = h2(t, fs)
	y = 1/5 * sinc(fs/5 * (t - 0.2));
return

function y = h3(t, fs)
	y = 1/5 * sinc(fs/5 * (t - 0.05));
return
