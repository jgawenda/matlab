function lti_rek()
	% wyznaczanie odpowiedzi impulsowej dla zadanych transmitancji
	% zapisywanie współczynników filtrów rekursywnych
	% bx - współczynniki licznika
	% ax - współczynniki mianownika
	b1 = [2 3];
	a1 = [1 -1/2];

	b2 = [2 3];
	a2 = [1 1/2];

	b3 = [2 -1];
	a3 = [1 3/2 -1/2];

	b4 = [2 -1];
	a4 = [1 1/2 -1/2];

	% obliczanie odpowiedzi impulsowej za pomocą funkcji wbudowanej impz()
	[h1, t1] = impz(b1, a1, 50);
	[h2, t2] = impz(b2, a2, 50);
	[h3, t3] = impz(b3, a3, 50);
	[h4, t4] = impz(b4, a4, 50);
	
	% obliczanie odpowiedzi za pomocą równania różniczkowego
	% zapisuję pierwsze dwie wartości dla y(1) = 2*x(1) i y(2) = 3*x(1) + 1/2*y(1)
	% to jest po to, żeby potem łatwiej liczyć odpowiedź (bo zostaje tylko odpowiedź y(n) = 1/2 * y(n-1)
	h1m = [2, 4];
	for i = 3:50
		h1m = [h1m, 1/2*h1m(i-1)];
	end

	% rysowanie odpowiedzi impulsowej dla wszystkich transmitancji
	figure
	subplot(4, 1, 1)
	title('odpowiedź stabilna')
	hold on;
	plot(t1, h1);

	subplot(4, 1, 2)
	title('odpowiedź stabilna')
	hold on;
	plot(t2, h2);

	subplot(4, 1, 3)
	title('odpowiedź niestabilna')
	hold on;
	plot(t3, h3);

	subplot(4, 1, 4)
	title('odpowiedź na granicy stabilności')
	hold on;
	plot(t4, h4);
return
