function zmiana_f()
	% program do zamiany częstotliwości próbkowania
	% możecie się bawić jak chcecie, zmieniając jedynie liczby w funkcjach
	fs = 1000;
	load('zczpr.mat');

	% zamiana indeksów na indeksy czasowe
	n = n/fs; 
	% jakaś stała, żeby nie brać wszystkich próbek - bo wychodzą poza rysunek sygnału analogowego
	N = 80;

	% wycinanie części próbek
	x = x(1:N);
	n = n(1:N);

	% zastosowanie funkcji do nad- i podpróbkowania do zmiany częstotliwości
	[x_nad n_nad] = nadprobkowanie(x, n, 3);
	[x_pod n_pod] = podprobkowanie(x_nad, n_nad, 2);

	% obliczanie widma sygnału, przed i po zmianie częstotliwości próbkowania
	% żeby pokazać, że funkcja działa
	X = fft(x);
	F = (0:N-1)*fs/N;

	X_POD = fft(x_pod);
	N_POD = length(x_pod);
	F_POD = (0:N_POD-1)*3/2*fs/N_POD;

	% rysowanie sygnału i próbek 
	figure, hold on, grid on;
		plot(t, s, 'b');
		stem(n, x, 'r');
		stem(n_pod, x_pod, 'g');

	% w tym wykresie widać, że widmo sygnału przed zmianą (niebieskie), jest skupione wokół częstotliwości fs/2 = 500 Hz
	% natomiast widmo sygnału po zmianie częstotliwości (czerwone) jest skupione wokół częstotliwości fs/2 * K/M = 750 Hz
	figure, hold on, grid on;
		stem(F, abs(X), 'b', 'fill');
		stem(F_POD, abs(X_POD), 'r', 'fill');
return

function [x_pod t_pod] = podprobkowanie(x, t, M)
	% --------------- WEJŚCIA --------------
	% x - wektor próbek
	% t - wektor indeksów czasowych
	% M - stała decymatora
	% --------------- WYJŚCIA --------------
	% x_pod - wektory próbek po podpróbkowaniu
	% t_pod - wektor ideksów czasowych po podpróbkowaniu
	
	N = length(x);

	% obliczanie nowej odległości między próbkanmi. Przy podpróbkowaniu, zwiększa się
	dt = t(2) - t(1);
	dt_pod = dt*M;

	% branie co M-tej próbki
	x_pod = x(1:2:N);
	N_pod = length(x_pod);
	t_pod = t(1):dt_pod:t(end);
return

function [x_nad t_nad] = nadprobkowanie(x, t, K)
	% --------------- WEJŚCIA -------------
	% x - wektor próbek
	% t - wektor indeksów czasowych
	% K - stała ekspandera
	% --------------- WYJŚCIA -------------
	% x_nad - wektor próbke po nadpróbkowaniu
	% t_nad - wektor indeksów czasowych po nadpróbkowaniu

	% rząd filtru interpolatora
	R = 90;

	N = length(x);

	% obliczanie nowej odległości między próbkami. Przy nadpróbkowaniu, zminiejsza się
	dt = t(2) - t(1);
	dt_nad = dt/K;

	% generowanie nowego wektora próbek, z dodatkowymi zerami
	x_nad = [];
	for i = 1:N
		% między próbki i na koniec wstawiamy K-1 zer
		x_nad = [x_nad, x(i), zeros(1, K-1)];
	end

	N_nad = length(x_nad);

	% nowy wektor indeksów czasowych
	t_nad = t(1) : dt_nad : t(end) + (K-1)*dt_nad;

	% generowanie współczynników filtru interpolatora
	h = fir2(R, [0, 1/K, 1/K, 1], [1 1 0 0]);

	% filtrowanie próbek i wycinanie części sygnału w stanie ustalonym
	x_nad = conv(x_nad, h);
	x_nad = K*x_nad(1 + R/2 : length(t_nad) + R/2); 
return
