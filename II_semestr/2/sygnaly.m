function sygnaly()
	% fo - częstotliwość sygnału
	% fs - częstotliwość próbkowania
	%
	fo = 1;
	fs = 4;

	% sygnał "analogowy"
	t = linspace(0, 8, 1000);
	y = sin(2*pi*fo*t);

	% sygnał cyfrowy, próbki są oddalone od siebie o 1/fs czasu
	% jest lekko przesunięty o losową część 1/fs w prawo
	n = (0:1/fs:t(end-1)) + rand(1,1)/fs;
	x = sin(2*pi*fo*n);

	[ti, yi] = whittaker(n, x);

	% wykresy do pokazania
	figure, hold on, grid on;
	title('wykres sygnału analogowego, cyfrowego i interpolowanego')
	plot(t, y, 'b');
	stem(n, x, 'r');
	plot(ti, yi, 'g');

	figure, hold on, grid on;
	title('wykres sygnału cyfrowego i interpolowanego')
	stem(n, x, 'r');
	plot(ti, yi);
return

function [t, y] = whittaker(n, x)
	% funkcja interpolująca próbki za pomocą szeregu kardynalnego Whittakera
	% n - indeksy czasowe próbek
	% x - wartości próbek

	t = linspace(0, 8, 1000);
	dt = n(2) - n(1);

	y = t*0;
	% interpolacja próbek (wzór na pierwszym wykładzie Kowalczyka) 
	for i = 1:length(x)
		y = y + x(i)*sinc((t - n(i))/dt);
	end
return
