function sygnal()
	% widmo sygnału z funkcji Sypki
	[x, fs] = CPS_LAB04_sygnal(1);

	N = length(x);
	x = x.*(window(@blackman, N).');
	X = fft(x);
	F = (0:(N-1))*fs/N;

	figure, hold on, grid on;
	stem(F, abs(X));
return
