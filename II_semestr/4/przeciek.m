function przeciek()
	% obrazuje zjawisko przecieku na podstawie trzech sygnałów
	% tylko w ostatnim występuje przeciek
	N1 = 16; % liczba próbek
	N2 = 15;

	fs1 = 4000; % częstotliwości próbkowania
	fs2 = 3000;

	fo1 = 500; % częstotliwości sygnałów
	fo2 = 600;
	fo3 = 666;
	
	% generowanie sygnałów analogowych
	t1 = linspace(0, 2/fo1, 200);
	t2 = linspace(0, 3/fo2, 200);
	t3 = linspace(0, 4/fo3, 200);

	y1 = sin(2*pi*fo1*t1);
	y2 = sin(2*pi*fo2*t2);
	y3 = sin(2*pi*fo3*t3);

	% generowanie indeksów czasowych 
	n1 = 0:1/fs1:(N1-1)/fs1;
	n2 = 0:1/fs2:(N2-1)/fs2;

	% "próbkowanie" sygnału
	x1 = sin(2*pi*fo1*n1);
	x2 = sin(2*pi*fo2*n2);
	x3 = sin(2*pi*fo3*n2);

	% obliczanie transformaty Fouriera dla danych próbek
	% X1 - widmo dyskretne
	% X1C - widmo ciągłe 
	X1 = fft(x1);
	X1C = fft(x1, 4096);

	X2 = fft(x2);
	X2C = fft(x2, 4096);

	X3 = fft(x3);
	X3C = fft(x3, 4096);

	% generowanie indeksów częstotliwości dla danych dyskretnych widm
	F1 = (0:(N1-1))*fs1/N1;
	F2 = (0:(N2-1))*fs2/N2;

	% generowanie wektora częstotliwości dla widm ciągłych
	F1C = linspace(0, fs1, length(X1C));
	F2C = linspace(0, fs2, length(X2C));

	% rysowanie wyników
	figure, hold on, grid on;
		title('fo = 500, fs = 4000, N = 16, df = 250')
		plot(t1, y1);
		stem(n1, x1);

	figure, hold on, grid on;
		title('fo = 600, fs = 3000, N = 15, df = 200')
		plot(t2, y2);
		stem(n2, x2);

	figure, hold on, grid on;
		title('fo = 666, fs = 3000, N = 15, df = 200')
		plot(t3, y3);
		stem(n2, x3);
	
	figure, hold on, grid on;
		title('fo = 500, fs = 4000, N = 16, df = 250')
		plot(F1C, abs(X1C));
		stem(F1, abs(X1));

	figure, hold on, grid on;
		title('fo = 600, fs = 3000, N = 15, df = 200')
		plot(F2C, abs(X2C));
		stem(F2, abs(X2));

	% TU JEST PRZECIEK, BO 666 NIE JEST ŻADNĄ WIELOKROTNOŚCIĄ df
	figure, hold on, grid on;
		title('fo = 666, fs = 3000, N = 15, df = 200')
		plot(F2C, abs(X3C));
		stem(F2, abs(X3));
