function okno()
	% pokazuje wpływ okna na widmo sygnału
	% jeśli zmienicie częstotliwość na 600, w widmie sygnału po zastosowaniu okna zrobi się przeciek (?)
	fs = 3000; % częstotliwość próbkowania
	fo = 666; % częstotliwość sygnału
	N = 15; % liczba próbek

	% wektor czasu dla sygnału 'analogowego'
	t = linspace(0, 3/fo, 300);
	y = sin(2*pi*fo*t);

	% wektor indeksów czasowych, potrzebny do obliczenia próbek
	n = 0:1/fs:(N-1)/fs;
	x = sin(2*pi*fo*n);

	% rysowanie sygnału i jego próbek
	figure, hold on, grid on;
		title('wykres sygnału i jego próbek')
		plot(t, y, 'b');
		stem(n, x, 'r');

	% stosowanie okna Blackmana 
	yo = y.*(window(@blackman, length(y)).');
	xo = x.*(window(@blackman, length(x)).');

	figure, hold on, grid on;
		title('wykres sygnału po zastosowaniu okna Blackmana')
		% ciut się nie zgadzają, ale nawet Sypka nie wiedział dlaczego
		plot(t, yo, 'b');
		stem(n, xo, 'r');

	% generowanie widm dyskretnych i ciągłych dla próbek przed i po zastosowaniu okna
	X = fft(x);
	XC = fft(x, 3000);

	XO = fft(xo);
	XOC = fft(xo, 3000);

	% generowanie indeksów częstotliwości
	F = (0:(N-1))*fs/N;
	FC = linspace(0, fs, length(XC));

	% rysowanie wyników
	figure
		subplot(2, 1, 1);
		title('widmo sygnału bez zastosowania okna')
		hold on, grid on;
		plot(FC, abs(XC), 'b');
		stem(F, abs(X), 'r');

		subplot(2, 1, 2);
		title('widmo sygnału po zastosowaniu okna')
		hold on, grid on;
		plot(FC, abs(XOC), 'b');
		stem(F, abs(XO), 'r');
